
export const subtractDaysFromDate = (date: Date, days: number = 30) => {
    const generateDate = new Date(Number(date));
    return generateDate.setDate(date.getDate() - days);
}

