export enum Area {
    Administracion = 'ADMINISTRACION',
    Financiera = 'FINANCIERA',
    Compras = 'COMPRAS',
    Infraestructura = 'INFRAESTRUCTURA',
    Operacion = 'OPERACION',
    TalentoHumano = 'TALENTO HUMANO',
    ServiciosVarios = 'SERVICIOS VARIOS'
}