import { BadRequestException, ConflictException, Injectable, NotFoundException, Query } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, DataSource, Like } from 'typeorm';


import { CreateEmployeeDto } from './dto/create-employee.dto';
import { FindAllDto } from './dto/find-all.dto';
import { FindOneEmployeeDto } from './dto/find-one-employee.dto';
import { GetEmployeeByEmailDto } from './dto/get-employee-by-email.dto';
import { GetEmployeeByIdentificationDto } from './dto/get-employee-by-identification.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { Employee } from './entities/employee.entity';
import { Country } from 'src/enums/CountryEnum';
import { Dominio } from 'src/enums/DominioEnum';
import { subtractDaysFromDate } from 'src/util/validateDate.util';

@Injectable()
export class EmployeesService {

  constructor(
    @InjectRepository(Employee)
    private readonly employeeRepository: Repository<Employee>,
    private readonly dataSource: DataSource
  ) { }

  async create(createEmployeeDto: CreateEmployeeDto): Promise<Employee> {

    const { idNumber, firstSurname, firstName, country, admissionDate } = createEmployeeDto;

    const currentDate = new Date();

    const expirationTime = new Date(subtractDaysFromDate(currentDate, 30));

    if (expirationTime.getTime() > new Date(admissionDate).getTime()) {
        throw new ConflictException('admission date can just be one month early');
    }

    if (new Date(admissionDate).getTime() > currentDate.getTime()) {
      throw new ConflictException('admission date can not be greater than current date');
  }


    const employeeIdNumber = await this.getEmployeeByIdentification({ idNumber });



    if (employeeIdNumber) {
      throw new BadRequestException(`There is aready a user with identificacion: ${idNumber}`);
    }

    const queryRunner = this.dataSource.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    const employee = this.employeeRepository.create({ ...createEmployeeDto });

    try {
      let saved = await queryRunner.manager.save(employee);

      //This is how we generate a automated email
      const fullName = `${firstName}.${firstSurname}`.toLocaleLowerCase();
      let email = country === Country.Colombia ? `${fullName}@${Dominio.Col}` : `${fullName}@${Dominio.Usa}`;

      //
      const employeeEmail = await this.getEmployeeByEmail({ email });

      if (employeeEmail) {
        email = country === Country.Colombia ? `${fullName}.${saved.id}@${Dominio.Col}` : `${fullName}.${saved.id}@${Dominio.Usa}`;

      }

      const merge = this.employeeRepository.merge(employee, { email });

      saved = await queryRunner.manager.save(merge);

      await queryRunner.commitTransaction();

      return saved;
    } catch (error) {
      await queryRunner.rollbackTransaction();
      return error;
    } finally {
      await queryRunner.release();
    }
  }

  async getEmployeeByEmail(getEmployeeByEmailDto: GetEmployeeByEmailDto): Promise<Employee | null> {

    const { email } = getEmployeeByEmailDto;

    const employee = await this.employeeRepository.findOne({
      where: {
        email
      }
    })

    return employee || null;
  }

  async getEmployeeByIdentification(GetEmployeeByIdentificationDto: GetEmployeeByIdentificationDto): Promise<Employee | null> {

    const { idNumber } = GetEmployeeByIdentificationDto;

    const employee = await this.employeeRepository.findOne({
      where: {
        idNumber
      }
    })

    return employee || null;
  }


  async findAll(findAllDto: FindAllDto): Promise<[Employee[], number]> {

    const { limit = 10, skip = 0, ...filters } = findAllDto;

    let where: any = { ...filters };

    if (where.firstName) where = { ...filters, firstName: Like(`%${filters.firstName.toUpperCase()}%`) };
    if (where.otherName) where = { ...filters, otherName: Like(`%${filters.otherName.toUpperCase()}%`) };
    if (where.firstSurname) where = { ...filters, firstSurname: Like(`%${filters.firstSurname.toUpperCase()}%`) };
    if (where.secondSurname) where = { ...filters, secondSurname: Like(`%${filters.secondSurname.toUpperCase()}%`) };
    if (where.idType) where = { ...filters, idType: Like(`%${filters.idType.toUpperCase()}%`) };
    if (where.country) where = { ...filters, country: Like(`%${filters.country.toUpperCase()}%`) };
    if (where.email) where = { ...filters, email: Like(`%${filters.email.toUpperCase()}%`) };
    if (where.state) where = { ...filters, state: Like(`%${filters.state.toUpperCase()}%`) };


    return await this.employeeRepository.findAndCount({
      take: limit,
      skip,
      order: {
        id: 'DESC'
      },
      where
    });
  }

  async findOne(findOneEmployeeDto: FindOneEmployeeDto): Promise<Employee> {
    const { id } = findOneEmployeeDto;

    const employee = await this.employeeRepository.findOne({
      where: {
        id
      }
    });

    if (!employee) {
      throw new NotFoundException(`Employee does not exist`);
    }

    return employee;
  }

  async update(findOneEmployeeDto: FindOneEmployeeDto, updateEmployeeDto: UpdateEmployeeDto): Promise<Employee> {

    const { firstName, firstSurname, country, idNumber } = updateEmployeeDto;

    const employee = await this.findOne(findOneEmployeeDto);


    if (idNumber) {
      const employeeIdNumber = await this.getEmployeeByIdentification({ idNumber });

      if (employeeIdNumber.id !== employee.id) {
        throw new BadRequestException(`There is aready a user with identificacion: ${idNumber}`);
      }
    }

    let email: any;

    if (firstName || firstSurname || country) {

      //This is how we generate a automated email
      const fullName = `${firstName || employee.firstName}.${firstSurname || employee.firstSurname}`.toLocaleLowerCase();

      const emailEnumeration = employee.email.replace(/\D/g, '');

      if (emailEnumeration === '') {

        email = (country || employee.country) === Country.Colombia ? `${fullName}@${Dominio.Col}` : `${fullName}@${Dominio.Usa}`;

        const employeeEmail = await this.getEmployeeByEmail({ email });

        if (employeeEmail && employeeEmail.id !== employee.id) {
          email = country === Country.Colombia ? `${fullName}.${employee.id}@${Dominio.Col}` : `${fullName}.${employee.id}@${Dominio.Usa}`;
        }

      } else {

        email = (country || employee.country) === Country.Colombia
          ? `${fullName}${emailEnumeration !== '' ? '.' + emailEnumeration : ''}@${Dominio.Col}`
          : `${fullName}${emailEnumeration !== '' ? '.' + emailEnumeration : ''}@${Dominio.Usa}`;
      }
    }

    const preload = await this.employeeRepository.preload({
      id: employee.id,
      ...updateEmployeeDto,
      email
    })

    return await this.employeeRepository.save(preload)
  }

  async remove(findOneEmployeeDto: FindOneEmployeeDto): Promise<Employee> {
    const employee = await this.findOne(findOneEmployeeDto);
    return await this.employeeRepository.remove(employee);
  }
}
