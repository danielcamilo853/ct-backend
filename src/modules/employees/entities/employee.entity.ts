import { Area } from "src/enums/AreaEnum";
import { Country } from "src/enums/CountryEnum";
import { IdentiifcationType } from "src/enums/IdentificationTypeEnum";
import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Employee {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({name: 'first_surname', length: 20})
    firstSurname: string;

    @Column({name: 'second_surname', length: 20})
    secondSurname: string;

    @Column({name: 'first_name', length: 20})
    firstName: string;

    @Column({name: 'other_name', length: 50, nullable: true})
    otherName: string;

    @Column()
    country: Country;

    @Column({name: 'id_type'})
    idType: IdentiifcationType;

    @Column({name: 'id_number', unique: true})
    idNumber: string; 

    @Column({length: 300, unique: true, nullable: true })
    email: string; 

    @Column({name: 'admission_date', nullable: true})
    admissionDate: Date;

    @Column()
    area: Area;

    @Column({default: 'activo'})
    state: string;

    @CreateDateColumn({name: 'created_at'})
    createdAt: Date;
    
    @UpdateDateColumn({name: 'updated_at'})
    updatedAt: Date;
}
