import { IsNotEmpty, IsNumberString } from "class-validator";

export class FindOneEmployeeDto {

    @IsNotEmpty()
    @IsNumberString()
    readonly id: number;
}