import { IsAlphanumeric, IsEmail, IsEnum, IsNumber, IsNumberString, IsOptional, IsString, Matches, MaxLength } from "class-validator";
import { Country } from "src/enums/CountryEnum";
import { IdentiifcationType } from "src/enums/IdentificationTypeEnum";

export class FindAllDto {

    @IsOptional()
    @IsString()
    @MaxLength(20)
    readonly firstName?: string;

    @IsOptional()
    @IsString()
    @MaxLength(50)
    readonly otherName?: string;

    @IsOptional()
    @IsString()
    @MaxLength(20)
    readonly firstSurname?: string;

    @IsOptional()
    @IsString()
    @MaxLength(20)
    readonly secondSurname?: string;

    @IsEnum(Country)
    @IsOptional()
    @IsString()
    readonly country?: Country;

    @IsEnum(IdentiifcationType)
    @IsOptional()
    @IsString()
    readonly idType?: IdentiifcationType;

    @IsOptional()
    @IsAlphanumeric()
    @MaxLength(20)
    readonly idNumber?: string;  

    @IsOptional()
    @IsEmail()
    @MaxLength(300)
    readonly email?: string;  


    @IsOptional()
    readonly state?: string;  

    @IsNumberString()
    @IsOptional()
    readonly limit: number;

    @IsNumber()
    @IsOptional()
    readonly skip: number;


}