import { IsAlphanumeric, IsNotEmpty, MaxLength } from "class-validator";

export class GetEmployeeByIdentificationDto {

    @IsNotEmpty()
    @IsAlphanumeric()
    @MaxLength(20)
    readonly idNumber: string; 
}