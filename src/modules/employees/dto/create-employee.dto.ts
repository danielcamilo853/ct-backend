import { IsAlphanumeric, IsDate, IsDateString, IsEmail, IsEnum, IsNotEmpty, IsOptional, IsString, Matches, MaxLength } from "class-validator";
import { Area } from "src/enums/AreaEnum";
import { Country } from "src/enums/CountryEnum";
import { IdentiifcationType } from "src/enums/IdentificationTypeEnum";

export class CreateEmployeeDto {

    @IsNotEmpty()
    @IsString()
    @MaxLength(20)
    @Matches(/^[A-Z]+$/,{message: 'firstSurname only apccets capital letters A-Z'})
    readonly firstSurname: string;

    @IsNotEmpty()
    @IsString()
    @MaxLength(20)
    @Matches(/^[A-Z]+$/,{message: 'secondSurname only apccets capital letters A-Z'})
    readonly secondSurname: string;

    @IsNotEmpty()
    @IsString()
    @MaxLength(20)
    @Matches(/^[A-Z]+$/,{message: 'firstName only apccets capital letters A-Z'})
    readonly firstName: string;

    @IsOptional()
    @IsString()
    @MaxLength(50)
    readonly otherName: string;

    @IsEnum(Country)
    @IsNotEmpty()
    @IsString()
    readonly country: Country;

    @IsEnum(IdentiifcationType)
    @IsNotEmpty()
    @IsString()
    readonly idType: IdentiifcationType;

    @IsNotEmpty()
    @IsAlphanumeric()
    @MaxLength(20)
    readonly idNumber: string;  

    @IsOptional()
    @IsDateString()
    readonly admissionDate: Date;

    @IsEnum(Area)
    @IsNotEmpty()
    @IsString()
    readonly area: Area;

}
