import { IsEmail, IsNotEmpty, MaxLength } from "class-validator";

export class GetEmployeeByEmailDto {

    @IsNotEmpty()
    @IsEmail()
    @MaxLength(300)
    readonly email: string; 
}