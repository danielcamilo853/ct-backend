import { Controller, Get, Post, Body, Param, Delete, Query, Put } from '@nestjs/common';

import { EmployeesService } from './employees.service';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { Employee } from './entities/employee.entity';
import { FindAllDto } from './dto/find-all.dto';
import { FindOneEmployeeDto } from './dto/find-one-employee.dto';

@Controller('employees')
export class EmployeesController {
  constructor(private readonly employeesService: EmployeesService) {}

  @Post()
  create(@Body() createEmployeeDto: CreateEmployeeDto): Promise<Employee> {
    return this.employeesService.create(createEmployeeDto);
  }

  @Get()
  findAll(@Query() findAllDto: FindAllDto): Promise<[Employee[], number]> {
    return this.employeesService.findAll(findAllDto);
  }

  @Get(':id')
  findOne(@Param()findOneEmployeeDto: FindOneEmployeeDto): Promise<Employee> {
    return this.employeesService.findOne(findOneEmployeeDto);
  }

  @Put(':id')
  update(@Param() findOneEmployeeDto: FindOneEmployeeDto, @Body() updateEmployeeDto: UpdateEmployeeDto): Promise<Employee> {
    return this.employeesService.update(findOneEmployeeDto, updateEmployeeDto);
  }

  @Delete(':id')
  remove(@Param() findOneEmployeeDto: FindOneEmployeeDto): Promise<Employee> {
    return this.employeesService.remove(findOneEmployeeDto);
  }
}
