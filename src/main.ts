import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();

  app.useGlobalPipes(new ValidationPipe({
    whitelist: true,
    forbidNonWhitelisted: true /*Alertar de un error al cliente cuando se envie un atributo no definido en el DTO*/,
  }));

  
  await app.listen(5000);
}
bootstrap();
